package controllers;


import hangman.HangmanManager;
import play.Configuration;
import play.Play;
import play.mvc.Controller;
import play.mvc.Result;
import twitter.listeners.HangmanStatusListener;
import twitter4j.FilterQuery;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import views.html.index;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Application extends Controller {
    private static long TWITTER_ID = 3129566927l;

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result twitterStream() {
        final ConfigurationBuilder cb = new ConfigurationBuilder();


        Configuration oauthConfig = play.Configuration.root().getConfig("oauth");
        cb.setDebugEnabled(false)
            .setOAuthConsumerKey(oauthConfig.getString("consumerKey"))
            .setOAuthConsumerSecret(oauthConfig.getString("consumerSecret"))
            .setOAuthAccessToken(oauthConfig.getString("accessToken"))
            .setOAuthAccessTokenSecret(oauthConfig.getString("accessTokenSecret"));

        twitter4j.conf.Configuration twitterConfig = cb.build();
        TwitterStream twitterStream = new TwitterStreamFactory(twitterConfig).getInstance();
        TwitterFactory twitterFactory = new TwitterFactory(twitterConfig);

        List<File> hangmanImageFiles = getHangmanImages();
        HangmanManager hangmanManager = new HangmanManager();
        twitterStream.addListener(new HangmanStatusListener(twitterFactory.getInstance(), hangmanImageFiles, hangmanManager));

        // filter() method internally creates a thread which manipulates TwitterStream and calls these adequate listener methods continuously.
        twitterStream.filter(new FilterQuery(0, new long[] { TWITTER_ID }));

        return ok(index.render("Your twitter stream is connected"));
    }

    private static List<File> getHangmanImages() {
        List<File> files = new ArrayList<>();

        files.add(Play.application().getFile("public/images/hangman/hang0.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang1.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang2.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang3.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang4.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang5.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang6.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang7.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang8.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang9.gif"));
        files.add(Play.application().getFile("public/images/hangman/hang10.gif"));

        return files;
    }

}
