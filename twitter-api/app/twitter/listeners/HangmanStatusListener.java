package twitter.listeners;

import hangman.Hangman;
import hangman.HangmanManager;
import hangman.State;
import org.apache.commons.lang3.StringUtils;
import twitter4j.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by matt.ramsay on 30/03/2015.
 */
public class HangmanStatusListener implements StatusListener {
    private Twitter twitter;
    private List<File> hangmanImages;
    private HangmanManager hangmanManager;
    private static String twitterHandle = "TalkinHangman";

    public HangmanStatusListener(Twitter twitter, List<File> hangmanImages, HangmanManager hangmanManager) {
        this.twitter = twitter;
        this.hangmanImages = hangmanImages;
        this.hangmanManager = hangmanManager;
    }

    @Override
    public void onStatus(twitter4j.Status status) {
        System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText());

        String command = status.getText().replace("@" + twitterHandle, StringUtils.EMPTY).trim();
        String player = status.getUser().getScreenName();
        if (StringUtils.equals(player, twitterHandle)) {
            return;
        }

        if (StringUtils.equalsIgnoreCase(command, "new game")) {
            try {
                Hangman game = hangmanManager.newGame(player);

                //TODO: write to storage game and player
                String gameStr = game.getGameString();
                String message = String.format("%s Good luck @%s - you'll need it!", gameStr, player);
                StatusUpdate statusUpdate = new StatusUpdate(message);
                statusUpdate.setMedia(hangmanImages.get(game.getIncorrectGuesses()));

                twitter.updateStatus(statusUpdate);
            } catch (TwitterException | IOException e) {
                e.printStackTrace();
            }
        } else {
            //Must be trying a letter then...
            try {
                Hangman game = hangmanManager.getGameByUser(player);
                String gameMessage = new String();
                if (command.length() == 1) {
                    gameMessage = game.guessLetter(command.charAt(0));
                } else {
                    gameMessage = game.guess(command);
                }

                State gameState = game.getState();
                String message = String.format("%s %s @%s", gameMessage, gameState.getMessage(), player);
                StatusUpdate statusUpdate = new StatusUpdate(message);
                statusUpdate.setMedia(hangmanImages.get(game.getIncorrectGuesses()));

                twitter.updateStatus(statusUpdate);

                if (gameState.getStatus().equals(State.Status.LOSE) || gameState.getStatus().equals(State.Status.WIN)) {
                    game = null;
                }
                hangmanManager.saveGame(player, game);
            } catch (TwitterException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
    }

    @Override
    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
        System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
    }

    @Override
    public void onScrubGeo(long userId, long upToStatusId) {
        System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
    }

    @Override
    public void onStallWarning(StallWarning warning) {
        System.out.println("Got stall warning:" + warning);
    }

    @Override
    public void onException(Exception ex) {
        ex.printStackTrace();
    }
}
