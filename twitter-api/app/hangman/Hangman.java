package hangman;


import java.io.IOException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rory.payne on 30/03/2015.
 */
public class Hangman {

    private Secret secret;
    private int incorrectGuesses;
    private String gameString;
    private String guessString;
    private State state;

    public Hangman(Secret secret) {
        this.secret = secret;
        this.incorrectGuesses = 0;
        this.gameString = initGameString(secret.getWord().length());
    }

    private String initGameString(int length) {
        String str = "";
        String word = this.secret.getWord();
        for (int i = 0 ; i < length ; i++) {
            if(StringUtils.equals(word.substring(i, i + 1), " ")){
                str += "  ";
            }else {
                str += "_ ";
            }
        }
        return str + appendCategory(secret.getCategory());
    }

    public String guessLetter(char guess) throws IOException {
        List<Integer> indexes = getIndexesOfGuess(guess);
        if (indexes.size() == 0) {
            incorrectGuesses ++;
            if (incorrectGuesses > 9) {
                gameString = "You Lose. Answer = " + secret.getWord();
                setState(State.Status.LOSE);
            } else {
                setState(State.Status.NEGATIVE);
            }
        } else {
            for (Integer i : indexes) {
                gameString = gameString.substring(0, 2 * i) + guess + gameString.substring((2 * i) + 1);
                guessString = gameString.substring(0,i) + guess + gameString.substring((i) + 1);
            }
            if (guessString.equals(secret.getWord())) {
                setState(State.Status.WIN);
            } else {
                setState(State.Status.POSITIVE);
            }
        }
        return gameString;
    }

    private String appendCategory(String category) {
        return "(" + category + ")";
    }

    private List<Integer> getIndexesOfGuess(char guess) {
        List<Integer> indexes = new ArrayList<>();
        int i = secret.getWord().indexOf(guess);
        while (i > -1) {
            indexes.add(i);
            i = secret.getWord().indexOf(guess, i + 1);
        }
        return indexes;
    }

    public boolean hasWon(){
        return secret.getWord() == guessString;
    }

    public String guess(String myguess){
        guessString = myguess;
        return guessString;
    }
    public Secret getSecret() {
        return secret;
    }

    public int getIncorrectGuesses() {
        return incorrectGuesses;
    }

    public String getGameString() {
        return gameString;
    }

    public State getState() {
        return state;
    }

    public void setState(State.Status context) throws IOException {
        this.state = new State(context, InsultFactory.getInsult(context));
    }
}
