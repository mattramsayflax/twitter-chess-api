package hangman;

/**
 * Created by rory.payne on 30/03/2015.
 */
public class State {

    private Status status;
    private String message;

    public State(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public enum Status {
        POSITIVE,
        NEGATIVE,
        WIN,
        LOSE
    }
}
