package hangman;

/**
 * Created by rory.payne on 30/03/2015.
 */
public class Secret {
    private String word;
    private String category;

    public Secret(String word, String category) {
        this.word = word;
        this.category = category;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
