package hangman;

/**
 * Created by rory.payne on 30/03/2015.
 */
public class Insult {
    private String context;
    private String message;

    public Insult(String context, String message) {
        this.context = context;
        this.message = message;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
