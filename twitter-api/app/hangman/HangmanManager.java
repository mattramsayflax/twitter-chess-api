package hangman;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by matt.ramsay on 30/03/2015.
 */
public class HangmanManager {

    private HashMap<String,Hangman> games = new HashMap<>();
    private HangmanFactory gameGenerator = new HangmanFactory();

    public Hangman newGame(String userId) throws IOException {
        Hangman game = gameGenerator.generateHangman();
        games.put(userId, game);

        return game;
    }

    public Hangman getGameByUser(String userId){
        return games.get(userId);
    }

    public void saveGame(String userId,Hangman mygame){
        games.put(userId,mygame);
    }

}
