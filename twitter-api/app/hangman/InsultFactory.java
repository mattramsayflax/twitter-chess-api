package hangman;

import hangman.State.Status;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rory.payne on 30/03/2015.
 */
public class InsultFactory {

    private static File insultsFile = new File("C:\\code\\twitter-chess-api\\twitter-api\\data\\insults.csv");

    public static String getInsult(Status context) throws IOException {
        FileReader fileReader = new FileReader(insultsFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        List<String> positive = new ArrayList<>();
        List<String> negative = new ArrayList<>();
        List<String> win = new ArrayList<>();
        List<String> lose = new ArrayList<>();

        while ((line = bufferedReader.readLine()) != null) {
            String[] lineParts = line.split(",");
            switch(Status.valueOf(lineParts[0].toUpperCase())) {
                case POSITIVE:
                    positive.add(lineParts[1]);
                    break;
                case NEGATIVE:
                    negative.add(lineParts[1]);
                    break;
                case WIN:
                    win.add(lineParts[1]);
                    break;
                case LOSE:
                    lose.add(lineParts[1]);
                    break;
            }
        }


        switch(context) {
            case POSITIVE:
                return positive.get((int) (Math.random()*positive.size()));
            case NEGATIVE:
                return negative.get((int) (Math.random()*negative.size()));
            case WIN:
                return win.get((int) (Math.random()*win.size()));
            case LOSE:
                return lose.get((int) (Math.random()*lose.size()));
            default:
                return negative.get((int) (Math.random()*negative.size()));
        }
    }
}
