package hangman;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rory.payne on 30/03/2015.
 */
public class HangmanFactory {

    private final File secretsFile = new File("C:\\code\\twitter-chess-api\\twitter-api\\data\\hangman.csv");

    public Hangman generateHangman() throws IOException {
        return new Hangman(getRandomSecret());
    }

    private Secret getRandomSecret() throws IOException {
        FileReader fileReader = new FileReader(secretsFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        List<Secret> secrets = new ArrayList<>();
        while ((line = bufferedReader.readLine()) != null) {
            String[] lineParts = line.split(",");
            secrets.add(new Secret(lineParts[1].toLowerCase(), lineParts[0].toLowerCase()));
        }
        return secrets.get((int)(Math.random()*secrets.size()));
    }
}
