package Hangman;

import hangman.Secret;
import hangman.Hangman;
import junit.framework.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.fest.assertions.Assertions.*;

/**
 * Created by rory.payne on 30/03/2015.
 */
public class HangmanTest {

    @Test
    public void testInitialiseNewGame() {
        Secret dummySecret = new Secret("answer", "category");
        Hangman hangman = new Hangman(dummySecret);
        assertThat(hangman.getGameString()).isEqualTo("_ _ _ _ _ _ (category)");
    }

    @Test
    public void testGuess() throws IOException {
        Secret dummySecret = new Secret("answer", "category");
        Hangman hangman = new Hangman(dummySecret);
        String gameString = null;
        try {
            gameString = hangman.guessLetter('s');
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(gameString).isEqualTo("_ _ s _ _ _ (category)");
    }

    @Test
    public void testGuessFirstLetter() throws IOException {
        Secret dummySecret = new Secret("answer", "category");
        Hangman hangman = new Hangman(dummySecret);
        String gameString = null;
        try {
            gameString = hangman.guessLetter('a');
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(gameString).isEqualTo("a _ _ _ _ _ (category)");
    }

    @Test
    public void testGuessLastLetter() throws IOException {
        Secret dummySecret = new Secret("answer", "category");
        Hangman hangman = new Hangman(dummySecret);
        String gameString = null;
        try {
            gameString = hangman.guessLetter('r');
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(gameString).isEqualTo("_ _ _ _ _ r (category)");
    }

    @Test
    public void testOutOfGuesses() throws IOException {
        Secret dummySecret = new Secret("answer", "category");
        Hangman hangman = new Hangman(dummySecret);
        String gameString = "";
        for (int i = 0; i < 11 ; i ++) {
            try {
                gameString = hangman.guessLetter('j');
            } catch (IOException e) {
                e.printStackTrace();
            }
            assertThat(hangman.getIncorrectGuesses()).isEqualTo(i + 1);
        }
        assertThat(gameString).isEqualTo("You Lose. Answer = answer");
    }

    @Test
    public void testSpace() {
        Secret dummySecret = new Secret("an swer", "category");
        Hangman hangman = new Hangman(dummySecret);
        String gameString=hangman.getGameString();
        assertThat(gameString).isEqualTo("_ _  _ _ _ _ (category)");
    }

    @Test
    public void testWin() {
        Secret dummySecret = new Secret("answer", "category");
        Hangman hangman = new Hangman(dummySecret);
        assertThat(hangman.guess("answer")).isEqualTo(true);
    }

    @Test
    public void testLose() {
        Secret dummySecret = new Secret("answer", "category");
        Hangman hangman = new Hangman(dummySecret);
        assertThat(hangman.guess("wong")).isEqualTo(false);
    }

    @Test
    public void twoCorrectGuesses() throws IOException {
        Secret dummySecret = new Secret("answer", "category");
        Hangman hangman = new Hangman(dummySecret);
        String gameString = null;
        try {
            gameString = hangman.guessLetter('r');
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            gameString = hangman.guessLetter('a');
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(gameString).isEqualTo("a _ _ _ _ r (category)");
    }
}
