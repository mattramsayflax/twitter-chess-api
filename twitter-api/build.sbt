name := "twitter-api"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
)

libraryDependencies += "org.twitter4j" % "twitter4j-stream" % "4.0.3"

play.Project.playJavaSettings
